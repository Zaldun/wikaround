package fr.vint.wikibichons.wikibichons;

import org.apache.commons.io.FilenameUtils;

/**
 * Created by Theophile on 25/10/2016.
 */

public class JsonImage {

    private String title;
    private float lat;
    private float lon;

    public float getLatitude() {
        return lat;
    }

    public float getLongitude() {
        return lon;
    }

    public String getTitle() {
        String _title = title;

        if(_title.startsWith("File:")) {
            _title = _title.substring(5, _title.length());
        }

        _title = FilenameUtils.removeExtension(_title);

        return _title;
    }
}
