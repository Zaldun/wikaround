package fr.vint.wikibichons.wikibichons;

/**
 * Created by Theophile on 25/10/2016.
 */

public class JsonQuery {

    private String batchcomplete;
    private Query query;

    public String getBatchcomplete() {
        return batchcomplete;
    }

    public JsonImage[] getImages() {
        return query.getGeosearch();
    }
}
